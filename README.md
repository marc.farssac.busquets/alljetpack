# AllJetPack

An App programmed in Kotlin using all Jet Pack components

## Objective

Apply the GitFlow branching model in a simple android App, adding feature and release branches and merging it to master.
The App is not functional today, but the repository follows the branching model.

## Project description

There will be one branch for each of the components so that the development steps can be followed. However there won't be too much branching happening in parallel.

## Branching model

The git flow branching model will be deployed, using master, develop, release, hotfix, feature branches and tags.

## About this README

This file will be updated after merging each of the components / branches to the master branch. This will also be used to document what has been incorporated into the project.

## Components

### Foundation

Foundation components provide cross-cutting functionality like backwards compatibility, testing and Kotlin language support.

#### Android KTX

From start. Used to writte more concise, idiomatic Kotlin code

#### AppCompat

The App extends AppCompatActivity used as a base class for activities that use the Support Library action bar implementation

### Architecture Components

Used to design robust, testable and maintainable apps.

#### ViewModel

Manage UI-related data in a lifecycle-conscious way

### Behavior

### UI

UI Components provide widgets and helpers to simplify app development.

#### Fragments

A basic unit of UI.


## Author

This project has been started on April 7th by [Marc Farssac](mailto:marc.farssac@gmail.com)

## Acknowledgements

Thanks to the Android developer documentation and the Stackoverflow community.

## Copyright 2020 Marc Farssac
