package com.marcfarssac.alljetpack.ui.slideshow

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import com.marcfarssac.alljetpack.R
import com.marcfarssac.alljetpack.databinding.FragmentSlideshowBinding

class SlideshowFragment : Fragment() {

    private lateinit var slideshowViewModel: SlideshowViewModel

    override fun onCreateView(
            inflater: LayoutInflater,
            container: ViewGroup?,
            savedInstanceState: Bundle?
    ): View? {

        val binding: FragmentSlideshowBinding = DataBindingUtil.inflate(
            inflater, R.layout.fragment_slideshow, container, false
        )
        val viewModelFactory = SlideshowViewModelFactory()
        val galleryViewModel =
            ViewModelProviders.of(
                this, viewModelFactory
            ).get(SlideshowViewModel::class.java)
        binding.slideshowViewModel = galleryViewModel

        val root = inflater.inflate(R.layout.fragment_slideshow, container, false)
        val textView: TextView = root.findViewById(R.id.text_slideshow)
        galleryViewModel.text.observe(viewLifecycleOwner, Observer {
            textView.text = it
        })

        return binding.root

    }
}
